<?php
namespace App\Presenters;

use Nette;

class Error4xxPresenter extends Nette\Application\UI\Presenter {

  public function startup()	{
		parent::startup();
		$request = $this->getRequest();
		if ($request === NULL || !$request->isMethod(Nette\Application\Request::FORWARD)) {
			$this->error();
		}
	}


	public function renderDefault(Nette\Application\BadRequestException $exception)	{
    if ($this->template instanceof Nette\Application\UI\ITemplate) {
      $file = __DIR__ . "/templates/Error/{$exception->getCode()}.latte";
      $this->template->setFile(is_file($file) ? $file : __DIR__ . '/templates/Error/4xx.latte');
    }
	}

}
